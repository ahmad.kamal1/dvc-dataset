import os
import shutil

proj_folder = '../'

for root, dirs, files in os.walk(proj_folder):
    if 'dvc-dataset' not in root:
        for filename in files:
            if filename.endswith('.dvc'):
                src_path = os.path.join(root, filename)
                dst_path = os.path.join(root[1:], filename)

                if not os.path.exists(os.path.dirname(dst_path)):
                    os.makedirs(os.path.dirname(dst_path))

                shutil.copy(src_path, dst_path)

